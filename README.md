# ipa-keyboard

Javascript library for a popup keyboard with the symbols of the International Phonetic Alphabet.

It is based on the IPA Chart http://www.internationalphoneticassociation.org/content/ipa-chart, available under a Creative Commons Attribution-Sharealike 3.0 Unported License. Copyright © 2015 International Phonetic Association.

### Build

This project uses CoffeeScript, Stylus and Pug to generate the necessary Javascript, CSS and HTML. It also uses Gulp for tasks.

1. `npm install`
1. `gulp`

### Contributing to this project

Want to help? I can use all the help I can get.

1. Fork it
1. Make your changes on a topic branch
1. Create new a pull request
1. Receive many thank-yous
