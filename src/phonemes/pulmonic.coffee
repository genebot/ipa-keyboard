Keyboard::phonemes ?= {}
Keyboard::phonemes.pulmonic = [
  {
    ipa: 'p'
    voicing: 'voiceless'
    place: 'bilabial'
    manner: 'plosive'
  }
  {
    ipa: 'b'
    voicing: 'voiced'
    place: 'bilabial'
    manner: 'plosive'
  }
  {
    ipa: 't'
    voicing: 'voiceless'
    place: 'alveolar'
    manner: 'plosive'
  }
  {
    ipa: 'd'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'plosive'
  }
  {
    ipa: 'ʈ'
    voicing: 'voiceless'
    place: 'retroflex'
    manner: 'plosive'
  }
  {
    ipa: 'ɖ'
    voicing: 'voiced'
    place: 'retroflex'
    manner: 'plosive'
  }
  {
    ipa: 'c'
    voicing: 'voiceless'
    place: 'palatal'
    manner: 'plosive'
  }
  {
    ipa: 'ɟ'
    voicing: 'voiced'
    place: 'palatal'
    manner: 'plosive'
  }
  {
    ipa: 'k'
    voicing: 'voiceless'
    place: 'velar'
    manner: 'plosive'
  }
  {
    ipa: 'g'
    voicing: 'voiced'
    place: 'velar'
    manner: 'plosive'
  }
  {
    ipa: 'q'
    voicing: 'voiceless'
    place: 'uvular'
    manner: 'plosive'
  }
  {
    ipa: 'ɢ'
    voicing: 'voiced'
    place: 'uvular'
    manner: 'plosive'
  }
  {
    ipa: 'ʔ'
    voicing: 'voiceless'
    place: 'glottal'
    manner: 'plosive'
  }
  {
    ipa: 'm'
    voicing: 'voiced'
    place: 'bilabial'
    manner: 'nasal'
  }
  {
    ipa: 'ɱ'
    voicing: 'voiced'
    place: 'labiodental'
    manner: 'nasal'
  }
  {
    ipa: 'n'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'nasal'
  }
  {
    ipa: 'ɳ'
    voicing: 'voiced'
    place: 'retroflex'
    manner: 'nasal'
  }
  {
    ipa: 'ɲ'
    voicing: 'voiced'
    place: 'palatal'
    manner: 'nasal'
  }
  {
    ipa: 'ŋ'
    voicing: 'voiced'
    place: 'velar'
    manner: 'nasal'
  }
  {
    ipa: 'ɴ'
    voicing: 'voiced'
    place: 'uvular'
    manner: 'nasal'
  }
  {
    ipa: 'ʙ'
    voicing: 'voiced'
    place: 'bilabial'
    manner: 'trill'
  }
  {
    ipa: 'r'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'trill'
  }
  {
    ipa: 'ʀ'
    voicing: 'voiced'
    place: 'uvular'
    manner: 'trill'
  }
  {
    ipa: 'ⱱ'
    voicing: 'voiced'
    place: 'labiodental'
    manner: 'flap'
  }
  {
    ipa: 'ɾ'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'flap'
  }
  {
    ipa: 'ɽ'
    voicing: 'voiced'
    place: 'retroflex'
    manner: 'flap'
  }
  {
    ipa: 'ɸ'
    voicing: 'voiceless'
    place: 'bilabial'
    manner: 'fricative'
  }
  {
    ipa: 'β'
    voicing: 'voiced'
    place: 'bilabial'
    manner: 'fricative'
  }
  {
    ipa: 'f'
    voicing: 'voiceless'
    place: 'labiodental'
    manner: 'fricative'
  }
  {
    ipa: 'v'
    voicing: 'voiced'
    place: 'labiodental'
    manner: 'fricative'
  }
  {
    ipa: 'θ'
    voicing: 'voiceless'
    place: 'dental'
    manner: 'fricative'
  }
  {
    ipa: 'ð'
    voicing: 'voiced'
    place: 'dental'
    manner: 'fricative'
  }
  {
    ipa: 's'
    voicing: 'voiceless'
    place: 'alveolar'
    manner: 'fricative'
  }
  {
    ipa: 'z'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'fricative'
  }
  {
    ipa: 'ʃ'
    voicing: 'voiceless'
    place: 'postalveolar'
    manner: 'fricative'
  }
  {
    ipa: 'ʒ'
    voicing: 'voiced'
    place: 'postalveolar'
    manner: 'fricative'
  }
  {
    ipa: 'ʂ'
    voicing: 'voiceless'
    place: 'retroflex'
    manner: 'fricative'
  }
  {
    ipa: 'ʐ'
    voicing: 'voiced'
    place: 'retroflex'
    manner: 'fricative'
  }
  {
    ipa: 'ç'
    voicing: 'voiceless'
    place: 'palatal'
    manner: 'fricative'
  }
  {
    ipa: 'ʝ'
    voicing: 'voiced'
    place: 'palatal'
    manner: 'fricative'
  }
  {
    ipa: 'x'
    voicing: 'voiceless'
    place: 'velar'
    manner: 'fricative'
  }
  {
    ipa: 'ɣ'
    voicing: 'voiced'
    place: 'velar'
    manner: 'fricative'
  }
  {
    ipa: 'χ'
    voicing: 'voiceless'
    place: 'uvular'
    manner: 'fricative'
  }
  {
    ipa: 'ʁ'
    voicing: 'voiced'
    place: 'uvular'
    manner: 'fricative'
  }
  {
    ipa: 'ħ'
    voicing: 'voiceless'
    place: 'pharyngeal'
    manner: 'fricative'
  }
  {
    ipa: 'ʕ'
    voicing: 'voiced'
    place: 'pharyngeal'
    manner: 'fricative'
  }
  {
    ipa: 'h'
    voicing: 'voiceless'
    place: 'glottal'
    manner: 'fricative'
  }
  {
    ipa: 'ɦ'
    voicing: 'voiced'
    place: 'glottal'
    manner: 'fricative'
  }
  {
    ipa: 'ɬ'
    voicing: 'voiceless'
    place: 'alveolar'
    manner: 'lateral-fricative'
  }
  {
    ipa: 'ɮ'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'lateral-fricative'
  }
  {
    ipa: 'ʋ'
    voicing: 'voiced'
    place: 'labiodental'
    manner: 'approximant'
  }
  {
    ipa: 'ɹ'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'approximant'
  }
  {
    ipa: 'ɻ'
    voicing: 'voiced'
    place: 'retroflex'
    manner: 'approximant'
  }
  {
    ipa: 'j'
    voicing: 'voiced'
    place: 'palatal'
    manner: 'approximant'
  }
  {
    ipa: 'ɰ'
    voicing: 'voiced'
    place: 'velar'
    manner: 'approximant'
  }
  {
    ipa: 'l'
    voicing: 'voiced'
    place: 'alveolar'
    manner: 'lateral-approximant'
  }
  {
    ipa: 'ɭ'
    voicing: 'voiced'
    place: 'retroflex'
    manner: 'lateral-approximant'
  }
  {
    ipa: 'ʎ'
    voicing: 'voiced'
    place: 'palatal'
    manner: 'lateral-approximant'
  }
  {
    ipa: 'ʟ'
    voicing: 'voiced'
    place: 'velar'
    manner: 'lateral-approximant'
  }
]
