class Keyboard
  constructor: (@$el) ->
    @buildKeyboard()
    @fillTable()
    @setupMenu()
    @makeButtonsWork()

  buildKeyboard: ->
    $("body").append @keyboard

  setupMenu: ->
    $('.ipa-keyboard-menu-option').on 'click', (e) ->
      e.preventDefault()
      href = $(e.target).attr('href')
      keyboard = $(".ipa-keyboard#{href}")

      $(".ipa-keyboard").removeClass('active')
      $("#ipa-keyboard").css('width', keyboard.data('width') + 'px')
      keyboard.addClass('active')

      $('.ipa-keyboard-menu-option').removeClass('active')
      $(e.target).addClass('active')

  fillTable: ->
    @phonemes.pulmonic.forEach (phoneme) ->
      $(".ipa-keyboard.pulmonic .ipa-keyboard-cell.#{phoneme.manner}.#{phoneme.place} a.#{phoneme.voicing}").addClass('filled').text(phoneme.ipa)

    $(".ipa-keyboard:not(.pulmonic) .key.filled").each (i, key) =>
      glyphCode = $(key).attr('data-example') || $(key).attr('data-glyph')
      $(key).text @_glyphForCode(glyphCode)

  makeButtonsWork: ->
    $(".key:not(:empty)").on "click", (e) =>
      e.preventDefault()

      glyphCode = $(e.target).attr('data-glyph')
      if glyphCode
        glyph = glyphCode.split(' ').map (charCode) ->
          String.fromCharCode(parseInt(charCode, 16))
        .join('')
      else
        console.warn 'glyphCode is missing! Defaulting to text.'
        glyph = $(e.target).text()

      console.log 'glyph is', glyph
      @$el.val @$el.val() + glyph
      @$el.focus()

  _glyphForCode: (code) ->
    glyph = code.split(' ').map (charCode) ->
      String.fromCharCode(parseInt(charCode, 16))
    .join('')

$.fn.extend(ipaKeyboard: -> new Keyboard(@))
