gulp = require('gulp')
coffee = require('gulp-coffee')
concat = require('gulp-concat')
del = require('del')
dest = require('gulp-dest')
pug = require('gulp-pug')
stylus = require('gulp-stylus')
wrap = require('gulp-wrap')

gulp.task 'stylus', ->
  gulp.src 'src/ipa-keyboard.styl'
  .pipe stylus()
  .pipe gulp.dest('./dist/')

gulp.task 'pug', ->
  gulp.src 'src/templates/keyboard.pug'
  .pipe pug()
  .pipe wrap('Keyboard::keyboard = \'<%= contents %>\'')
  .pipe dest('src/templates', ext: '.coffee')
  .pipe gulp.dest('./')

gulp.task 'build', ['stylus', 'pug'], ->
  gulp.src ['src/ipa-keyboard.coffee', 'src/phonemes/*.coffee', 'src/templates/keyboard.coffee']
  .pipe concat('ipa-keyboard.coffee')
  .pipe coffee(bare: true, sourceMaps: true)
  .pipe gulp.dest('dist')

gulp.task 'clean', ['build'], ->
  del(['src/templates/keyboard.coffee'], silent: true)

gulp.task 'watch', ->
  gulp.watch './src/**/*', ['clean']

gulp.task 'default', ['watch']
